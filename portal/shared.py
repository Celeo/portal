import time

from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy()


class EVEAPICache(object):
    """ Memory-only implementation of a cache for eveapi. """

    def __init__(self):
        self.cache = {}

    def retrieve(self, host, path, params):
        """
            Called when eveapi wants to fetch a document.
            `host` is the address of the server, `path` is the full path to
            the requested document, and `params` is a dict containing the
            parameters passed to this api call (keyID, vCode, etc).
            The method MUST return one of the following types:
                None - if your cache did not contain this entry
                str/unicode - eveapi will parse this as XML
                Element - previously stored object as provided to store()
                file-like object - eveapi will read() XML from the stream.
        """
        key = hash((host, path, frozenset(params.items())))
        in_memory = self.cache.get(key, None)
        if not in_memory:
            return None
        if time.time() < in_memory[0]:
            return in_memory[1]
        else:
            del self.cache[key]
            return None

    def store(self, host, path, params, doc, obj):
        """
            Called when eveapi wants you to cache this item.
            You can use `obj` to get the info about the object (cachedUntil
            and currentTime, etc). `doc` is the XML document the object was
            generated from. It's generally best to cache the XML, not the object,
            unless you pickle the object. Note that this method will only
            be called if you returned None in the retrieve() for this object.
        """
        self.cache[hash((host, path, frozenset(params.items())))] = (obj.cachedUntil, doc)


    def __repr__(self):
        return '<EveAPICache>'


import eveapi
api = eveapi.EVEAPIConnection(cacheHandler=EVEAPICache())
