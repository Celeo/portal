from portal.shared import db
from datetime import datetime
import bcrypt


class User(db.Model):
    __tablename__ = 'portal_user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    password = db.Column(db.String(100))
    created = db.Column(db.DateTime)
    api_keys = db.relationship('ApiKey', backref='user', lazy='dynamic')
    primary_name = db.Column(db.String(100))
    primary_corp = db.Column(db.String(100))
    active = db.Column(db.Boolean)

    def __init__(self, name, password):
        self.name = name
        self.password = bcrypt.hashpw(password, bcrypt.gensalt())
        self.created = datetime.utcnow()
        self.primary_name = None
        self.primary_corp = None
        self.active = True

    def check_password(self, password):
        return bcrypt.checkpw(password, self.password)

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return '<User-{}-{}-{}>'.format(self.id, self.name, self.primary_name)


class ApiKey(db.Model):
    __tablename__ = 'portal_apikey'

    id = db.Column(db.Integer, primary_key=True)
    key_id = db.Column(db.BigInteger)
    v_code = db.Column(db.String(50))
    user_id = db.Column(db.Integer, db.ForeignKey('portal_user.id'))

    def __init__(self, user_id, key_id, v_code):
        self.user_id = user_id
        self.key_id = key_id
        self.v_code = v_code

    def __repr__(self):
        return '<ApiKey-{}-{}>'.format(self.key_id, self.v_code)
