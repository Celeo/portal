from flask import Flask, redirect, url_for, request, session, render_template, flash
from flask.ext.login import LoginManager, current_user, login_user, logout_user, login_required
from datetime import datetime, timedelta
from portal.shared import db, api
from portal.models import User, ApiKey


# flask
app = Flask(__name__)
app.config.from_pyfile('config.cfg')
app.permanent_session_lifetime = timedelta(days=14)

# flask-sqlalchemy
db.app = app
db.init_app(app)

# flask-login
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login_page'
login_manager.login_message_category = 'warning'
login_manager.session_protection = 'strong'


@app.route('/login', methods=('GET', 'POST'))
def login_page():
    """ View: Start oauth authorization """
    if request.method == 'POST':
        username, password = request.form.get('username', None), request.form.get('password', None)
        user = User.query.filter_by(name=username).first()
        if user:
            if user.check_password(password):
                login_user(user, remember=True)
                flash('Loged in as {}'.format(user.name), 'info')
                return redirect(url_for('index'))
            else:
                flash('Invalid username or password', 'error')
        else:
            flash('User not found', 'error')
    return render_template('login.html')


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/logout')
def logout_page():
    """ View: Delete the session cookie and redirect """
    logout_user()
    return redirect(url_for('login_page'))


@app.errorhandler(404)
def error_404(error):
    return render_template('_error_.html', message='404 error - page not found')


@app.errorhandler(500)
def error_500(error):
    return render_template('_error_.html', message='500 error - something broke')


@app.context_processor
def _prerender():
    """ Add variables to all templates """
    return dict(user=current_user)

# =================================================================================================

@app.route('/')
@login_required
def index():
    return render_template('index.html')


@app.route('/account')
@login_required
def account():
    return render_template('account.html')


@app.route('/apply')
def apply_home():
    return render_template('apply_home.html')


@app.route('/apply/check')
def apply_check():
    if not request.args.get('key_id') or not request.args.get('v_code'):
        flash('You must enter both a key id and verification code', 'error')
        return redirect(url_for('apply_home'))
    key_id, v_code = request.args.get('key_id'), request.args.get('v_code')
    auth = api.auth(keyID=key_id, vCode=v_code)
    try:
        key_info = auth.account.APIKeyInfo()
        if not key_info.key.type == 'Account':
            flash('Your key is not "Account". Please fix.', 'error')
            return redirect(url_for('apply_home'))
        if key_info.key.expires:
            flash('Your key expires. Please fix.', 'error')
            return redirect(url_for('apply_home'))
        if not key_info.key.accessMask == 268435455:
            flash('Incorrect access mask (needs to be full). Please fix.', 'error')
            return redirect(url_for('apply_home'))
        return redirect(url_for('apply_apply'), key_id=key_id, v_code=v_code)
    except Exception:
        flash('An error occurred when processing your key. Are you sure you have the correct mask set?', 'error')
        return redirect(url_for('apply_home'), **request.args)
    return render_template('apply_check.html')


@app.route('/apply/apply', methods=('POST'))
def apply_apply():
    # TODO
    key_id, v_code = request.form.get('key_id'), request.form.get('v_code')
    if not key_id or not v_code:
        flash('No API key information received', 'error')
        return redirect(url_for('apply_check'))
    auth = api.auth(keyID=key_id, vCode=v_code)
    characters_raw, characters = auth.account.Characters(), []
    for c in characters_raw.characters:
        characters.append({
            'name': c.characterName,
            'id': c.characterID,
            'corp': c.corporationName,
            'alliance': c.allianceName,
        })
    return render_template('apply_apply.html')


@app.route('/groups')
@login_required
def groups():
    # TODO
    return render_template('groups.html')


@app.route('/ping')
@login_required
def ping():
    # TODO
    return render_template('ping.html')


@app.route('/admin')
@login_required
def admin():
    # TODO
    return render_template('admin.html')
